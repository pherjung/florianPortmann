# 60 ans d’aventures en Amérique du Sud
## Le père Noël de Brasilia
De tous les personnages d’origine suisse que nous avons rencontrés dans le monde entier, Florian Portmann est incontestablement un des plus marquants, un des plus attachants aussi.
Florian, c’est l’homme le plus connu de Brasilia, le plus populaire ; c’est le père Noël.
Avec ses longs cheveux blancs, sa barbe fleurie, il est, pour les enfants de la capitale brésilienne, papa Noël tout au long de l’année.

## Bain de foule assuré
Une promenade avec lui sur les larges avenues de la ville sortie du néant il y a un quart de siècle,
ou dans les quartiers populaires des cités satellites, vous plonge inévitablement dans un étonnant bain de foule.
Une nuée de gosses courent vers le vieil homme en criant « papa Noël ! papa Noël ! » Ils se précipitent à sa rencontre, l’interpellent joyeusement en lui tendant la main.
S’ils font ce geste, ce n’est pas qu’ils attendent un cadeau, car ils savent très bien que Noël ce n’est qu’une fois l’an, et encore, les meilleures années…
Non, ils tendent les bras pour le toucher simplement, pour lui dire bonjour et recevoir sa bénédiction.
Ils blottissent leur tête contre le patriarche très digne qui, dans un geste biblique, pose une main sur leur crâne et les bénit.
Et la scène se répète dix fois, vingt fois sur le chemin de l’hôpital orthopédique de Brasilia où il s’en va comme chaque jour raconter des histoires aux pauvres gosses privés momentanément,
ou à vie, de leurs moyens de locomotion.

## Un roman d’aventure
À plus de 80 ans, Florian Portmann est la paix et la douceur incarnées, personnage de sagesse et d’amour au royaume des enfants.
Extraordinaire aboutissement d’une vie longtemps marquée par la violence et les convulsions, l’aventure et la mort, dans un Brésil ressemblant à s’y méprendre au far-west des films américains les plus durs.
L’histoire de sa vie est un roman d’aventure, aux incroyables rebondissements, ayant pour cadre ce Brésil en fusion qui n’en finissait pas d’accoucher de sa propre création.
Le pays de l’éternel avenir, dit-on ?
Florian y a vécu au présent ces soixante et quelques années passées.
Il y a exercé tous les métiers, de gaucho à chercheur d’or, et dans les coins les plus reculés de la pampa et de la forêt amazonienne, avant de sauter à pieds joints dans le monde moderne de Brasilia.
Son histoire, il nous l’a racontée par bribes.
La première fois, c’était en Suisse, durant l’été 1985.
Il était revenu dans ce pays qu’il avait quitté soixante ans plus tôt, et nous avions parcouru avec lui les campagnes de sa jeunesse :
« Ce qui a le plus changé, avoua-t-il, ce sont les fumiers : ils ne sont plus soignés comme dans le temps ! »
Pour le reste, le regard vif de l’octogénaire balayait sans surprise le pays retrouvé et ses habitants.
Il faut dire qu’en fait de changements, il en avait vécu d’autres et de plus spectaculaires au Brésil.
Ce retour au pays avait quelque chose de pathétique ; et son séjour de deux mois et demi chez son frère André était pour Florian totalement inattendu.
Il le devait à l’initiative, à la générosité et à la reconnaissance du personnel et des médecins d’un hôpital de Brasilia :
ces gens avaient fait une collecte pour offrir à leur Suiço préféré un voyage dans son pays natal.
Il avait accepté ce cadeau royal en bégayant d’émotion et avait pris l’avion pour Genève.

## À Saint-Saph : clients médusés
Avec lui, nous sommes allés un après-midi dans le vignoble de Lavaux, qu’il avait envie de revoir.
À l’auberge de l’Onde de Saint-Saphorin, cher à Gilles, il a commencé à raconter le Brésil des années trente.
Instants magiques…
L’allure du conteur, échappé de quelque légende lointaine,
son discours épique et torrentiel entremêlé d’expressions vaudoises et d’interjections portugaises,
les gestes amples de ses larges mains ouvertes et son poing s’écrasant soudain sur la table de bois pour souligner quelques vifs propos… durant deux tours d’horloge, que personne n’a vu passer,
tous les clients du bistrot sont restés muets et suspendus aux lèvres du fascinant personnage.
C’est tout juste s’ils n’ont pas applaudi quand Florian s’est levé en s’excusant : il était attendu pour le souper.
Dommage, on aurait bien voulu connaître la suite…
La suite, il nous l’a racontée deux ans plus tard chez lui, à Brasilia.
En fait, on a tout repris à zéro, histoire de vérifier l’authenticité de la narration.
Il faut saluer ici la mémoire du bonhomme, qui se souvient des scènes de sa vie jusque dans les moindres détails :
ne donne-t-il pas le prénom du père et le lieu d’origine des innombrables personnages rencontrés à tous les détours de sa vie tumultueuse ?
Avec les années, la difficulté pour lui est de réunir ou de mettre bout à bout, chronologiquement, les volumes entiers de ses souvenirs si nets.
Nous l’avons aidé à le faire tout en nous demandant constamment quelle était la part de la mémoire phénoménale et la part de la possible affabulation.
Tous les recoupements que nous avons pu faire nous ont convaincu de la fiabilité des souvenirs de Florian.
Et si ça n’avait pas été le cas ?
Franchement, cela n’aurait rien changé à l’intérêt de son histoire, mais il eût fallu l’écrire alors au conditionnel ou la réserver à un recueil de fictions !
Allez, en voiture, et accrochez-vous, car le chemin est bosselé et sinueux.
Florian Portmann est né en 1905 à Orbe ; il y a vécu jusqu’à 15 ans.
On se souvient de lui dans le village voisin de Bofflens, où, adolescent, il venait garder les vaches chez Justin Gonin.
À la fin de scolarité, il a été placé à Rances, chez un paysan.
Il devait travailler aussi dans les vignes de Lavaux.
Mais très vite il est parti à l’étranger, à Paris, comme aide-mécanicien dans la société Air Liquide.
« C’est là qu’il y a eu l’explosion ! » tonne l’octogénaire,
qui se porte comme un charme mais se plaint d’une surdité consécutive à cet accident survenu deux ou trois ans après son arrivée dans la capitale française.


## Explosion à Paris
« J’ai eu la membrane d’un tympan fendue ou complètement détendue par l’explosion de 18 litres d’air liquide.
Il faut savoir qu’il est conservé à moins 200 degrés et que,
s’il entre en contact avec un corps gras, il revient brusquement à son état gazeux, ce qui fait une énorme explosion, comme une bombe très puissante.
C’était un dimanche, on m’avait demandé de terminer un travail de maçonnerie et j’étais seul, juste derrière la colonne où l’on sépare les différents gaz de l’air,
l’argon, le krypton, le néon, l’oxygène et l’azote… »
La mémoire de Florian fonctionne à merveille et ses talents de conteur font qu’à l’écouter on est véritablement transporté dans le Paris des années 20.
Maints détails étoffent son discours, entretiennent le suspense ; et l’on peut rester ainsi des heures suspendu à ses lèvres.
« Un stagiaire sorti de l’école d’ingénieur, un dénommé Ribaux,
est venu me demander s’il y avait de l’air liquide dans la colonne : il lui en fallait 20 litres pour des expériences qu’il effectuait au laboratoire.
Je lui ai répondu affirmativement, lui ai montré le robinet et je suis retourné à mon travail.
Je l’ai bien vu arracher un brin de paille au balai qui était là, souillé d’huile, mais je n’ai pas imaginé qu’il allait déboucher l’entonnoir avec cette paille et utiliser l’entonnoir !
Je n’ai rien entendu : on a retrouvé sa tête à trois cents mètres, un bras d’un côté, une jambe de l’autre… ».
Moi j’étais tout étourdi. Le portier a surgi en criant : qu’est-ce qui s’est passé ?
Il n’y a plus de toit ! En effet, le toit de verre avait volé en éclats.
J’ai couru comme un fou, j’ai traversé le rio Seine et je suis rentré chez moi.
Pendant cinq jours j’ai été complètement déséquilibré.
Comme j’étais le seul témoin, le commissaire est venu tout de suite m’interroger,
mais j’étais à moitié cinglé et c’est au bout d’une semaine seulement que j’ai pu faire ma déposition au commissariat.
Mais pourquoi l’avez-vous laissé prendre un brin de paille au balai ? m’a demandé plusieurs fois le commissaire.
Je lui ai répondu : normalement l’air liquide se prend avec une bouteille spéciale isolée,
je ne pouvais pas deviner ce qu’il allait faire avec ce brin ; il aurait pu se curer les dents… »
L’accident d’Air Liquide a défrayé la chronique,
et les journaux l’ont relaté jusqu’en Suisse où le père de Florian s’est alarmé et a exigé son retour au pays.
Le jeune homme dut obtempérer, mais ce ne fut qu’un aller et retour :
il repartit aussitôt pour Paris pour travailler quelque temps comme livreur chez Felix Potin,
puis dans un grand magasin, les établissements Moreau et Durand.
Florian aura encore d’autres emplois à Paris, où il fêtera ses vingt ans.


## Le grand-père américain
C’est à ce moment-là que l’envie lui vient d’émigrer au Canada.
Il a déjà entrepris des démarches pour obtenir de la terre.
Il ne serait pas le premier dans sa famille à traverser l’océan.
Du côté maternel, son arrière-grand-père, un Durussel,
qui fut préfet du Val-de-Travers avant la Révolution neuchâteloise, avait dû prendre la poudre d’escampette et s’était établi outre-Atlantique,
au sud du lac Ontario, dans l’État de New York.
Il serait mort après avoir été blessé lors de la guerre de Sécession.
Son fils, le grand-père de Florian, né aux États-Unis,
a fait le voyage inverse pour venir s’installer dans le canton de Neuchâtel.
Il épousera une Parisod, qui lui donnera deux fils et trois filles, dont Julie, la mère de Florian.
Pas de grands voyageurs à signaler du côté paternel, mais un oncle qui l’avait accueilli à Paris.
Son grand-père, venu de Lucerne, avait travaillé dans les chantiers du Simplon et du Saint-Gothard,
avant d’acheter un domaine au Pays-d’Enhaut.
Son père Alfred et Julie eurent cinq garçons : Alfred, Fernand, Louis, Florian et André :
« Les trois aînés sont morts, lâche Florian.
Il ne me reste qu’André, de dix ans mon cadet. »


## Cabotage jusqu’à Sao Paulo
Florian ne partira pas au Canada.
Il a rencontré un jeune Suisse allemand qui connaît un peu le Brésil pour y être allé une fois,
qui lui vante les avantages du pays et lui propose d’y aller à deux.
Florian se laisse facilement convaincre : il va en Suisse faire ses adieux à sa famille puis s’embarque au Havre,
en décembre 1927, sur un caboteur, le Bagé.
Il est seul : son copain s’est désisté au dernier moment.
Mais qu’importe, Florian a tôt fait de nouer des contacts et de se lier d’amitié avec un Anglais, nommé Hardwich,
qui convoie des pur-sang pour le président du Jockey-Club de Rio, Lineo de Paula Machado.
Trois cents émigrants portugais sont du voyage : pas besoin de la méthode Assimil,
ni de l’immersion Berlitz, pour apprendre les premiers rudiments,
les plus grossiers évidemment, de la langue dominante au Brésil.
Le bateau fait de nombreuses escales,
chargeant et déchargeant dans chaque port : en Espagne,
au Portugal, à Dakar, puis à Perambuc au Brésil, Salvador, Vitoria, Rio.


## Vigne anglaise à tailler
Florian prolonge jusqu’à Sao Paulo : « Quand je suis arrivé à Saint-Paul, je n’avais pas de projets précis.
Deux compagnons de voyage m’ont présenté un hôtel à la rue Concession : il y avait tellement de punaises dans le matelas,
que j’ai dû dormir directement sur les ressorts, ce qui ne m’a pas empêché d’être complètement bouffé par les bêtes.
Le lendemain, le consulat suisse m’a envoyé au bureau de l’immigration de la gare du Bras,
où arrivaient les victimes de la sécheresse du Nordeste.
Tous ceux qui cherchaient du travail étaient envoyés à la frontière bolivienne pour y construire une route.
On m’a dissuadé d’aller là-bas, mais on m’a demandé si je savais tailler la vigne.
Et comment que je savais ! Ça a été mon premier travail au Brésil :
prendre soin de la petite vigne d’une pension anglaise qui n’avait pas été taillé depuis cinq ans.
Mais je ne suis pas resté longtemps là,
car la patronne s’est mise à me faire de l’œil et au bout d’une quinzaine elle était prête à mettre son vieux jardinier à la porte pour m’offrir sa place !
Je lui ai dit que je n’avais pas l’habitude de prendre mon pain de la bouche d’un autre,
surtout pas d’un vieux. Je lui ai demandé mon compte, une trentaine de milreis,
et je suis parti pour Araras. » Le milreis est l’unité monétaire qui prévalait au Brésil et qu’on retrouvera régulièrement dans la bouche de Florian.
Un compte de reis, correspondait à un millier de milreis.


## Lavage de boilles
Florian prend le train pour Araras, à 200 kilomètres au nord de Sao Paulo,
car son ami anglais Hardwich lui avait donné l’adresse d’un domaine de cette région, la fazenda Sao José.
Mais à la gare d’Araras,
un ingénieur glaronais qui venait prendre livraison d’échantillons de lait apprend qu’il est de nationalité suisse et l’invite à se présenter à la fabrique suisse installée à deux pas.
« J’ai pu rencontrer le directeur Streit, se souvient Florian,
lui dire que mon père avait travaillé chez Nestlé, à Vevey, puis à Orbe, où il était contremaître.
Il n’avait pas besoin d’ouvrier, mais pour me dépanner et me donner le temps de parler correctement la langue du pays,
il m’a trouvé un emploi de manœuvre en attendant mieux : déchargement des wagons, lavage des boilles. »
Florian jouera les étoiles filantes dans la voie lactée,
quittant les wagons de lait au bout d’un mois, au terme d’une dispute spectaculaire, première d’une longue série, avec un collègue de travail qui eut la malheureuse idée de l’arroser au jet.
Florian explose : « Je lui ai lancé la turbine de la centrifugeuse,
je suis allé demander mon compte et je suis parti ! »
Dans le monde du travail de l’époque, on ne faisait pas forcément dans la dentelle…


## Le beurre et le miel
L’homme n’est pas du genre à se laisser marcher sur les pieds, comme on le verra ;
cela lui permettra de rester en vie dans un monde brutal où il vaut mieux être craint que faire pitié,
tant il est vrai que cette dernière ne s’y exerce guère.
Florian y ajoute parfois le panache, mais jamais le calcul.
Ses ruptures de contrat, ses changements d’emplois sont tonitruants et non prémédités.
Mais il retombe sur ses pattes, comme cette fois-ci.
Recommandé par son ami Hardwich, il est engagé à la fazenda Sao José,
située à mi-chemin entre Araras et Rio Claro.
« Le gérant, un Belge nommé Ledan, m’a proposé de travailler pour lui : faire du beurre.
J’ai installé de nouvelles machines, écrémeuse, malaxeur, batteuse,
et j’ai fait 25 kilos de beurre par jour comme j’avais appris à le faire quand j’étais jeune dans les alpages jurassiens, aux Dénériaz près de Sainte-Croix.
J’ai commencé aussi à m’occuper d’abeilles et à faire du miel. »
Comme tous les producteurs suisses de lait exerçant leurs talents sur les alpages d’Appenzell ou les pâturages d’outre-mer,
Florian analyse chaque jour le lait de chaque vache, sélectionne, fait couvrir ses meilleures vaches par le meilleur taureau du coin et s’efforce ainsi d’améliorer constamment la production.
En un mot, tout va bien dans le meilleur des mondes de l’élevage et Florian est même rejoint,
à sa demande, par son frère Louis, qui fait le voyage en convoyant des vaches bordelaises et des taureaux pour le grand patron des fazendas, Lineo de Paula de Machado.
Malheureusement, le gérant belge, le bon monsieur Ledan âgé de 78 ans, a passé la main,
cédant la gérance à un Italien, Eduardo Zucchari,
dont la première intervention est de vendre à un compatriote marchand de mortadelle toutes les vaches portantes de Florian,
sans le consulter, sous prétexte qu’elles allaient être victimes de la sécheresse :
« Ce n’était pas vrai, car j’avais trois énormes meules de réserve de foin, s’exclame Florian, très énervé en y pensant soixante ans plus tard.
Là-dessus, il m’a proposé d’aller m’occuper d’abeilles dans une autre fazenda, celle de Paula Suz à Itipucatu, prétendument très favorable.
Il me promettait 350 essaims.
Mon frère, lui, devait continuer à s’occuper de la laiterie,
l’élevage des poulains pur-sang exigeant toujours du lait écrémé. »
En réalité l’endroit n’est pas bon du tout, car trop exposé au vent ; il doit en chercher un autre.
Il construit un hangar, fabrique les ruches nécessaires avec des caisses de gazoline, les équipe de cadres mobiles.
Il transfère les trente ruches existantes, très sommaires.
Quand tout est prêt, il va à Sao José chercher les 350 essaims promis.
Que croyez-vous qu’il arriva ?
« Zucchari rechignait de me donner ces essaims, sous prétexte que la récolte avait été insuffisante depuis que je m’en occupais.
Il m’a insulté, je lui ai répondu.
On en est venu aux mains et je l’ai juste un peu esquinté. »
Les rapports de travail s’arrêtèrent évidemment là.
L’administrateur de la fazenda conseilla vivement à Florian de quitter les lieux,
proposant un charretier pour transporter ses bagages à la gare de Paula Suz.
Un Allemand, le marquis Langbern, qui travaillait non loin de là, lui suggéra d’aller s’installer dans la région de Mandouri, où il y avait des fermes à vendre.
Mais Florian n’entendait pas quitter les lieux sans récupérer les trois comptes de reis, une jolie somme qu’il avait versée pour sceller le contrat qui le liait à la fazenda.
Il voulait même porter plainte pour non-respect de ce contrat puisque les 350 essaims ne lui avaient pas été livrés.
Un ami policier examina le document et l’engagea à ne pas entamer de procédure :
« Le contrat ne vaut rien, lui expliqua-t-il ; les timbres qu’il porte ne sont pas des timbres officiels, mais des timbres de consommation ! »
En l’absence de Zucchari, cloué au lit, l’administrateur était sur le quai pour s’assurer qu’il partait bien ;
et il croyait s’en tirer sans bourse délier.
« Il m’a lancé : pas question de vous redonner cet argent ! se souvient Florian, franchement hilare.
L’administrateur a changé de ton et il s’est mis à trembler quand il a senti dans ses reins le canon du pistolet de mon frère Louis.
Il a donné les instructions nécessaires et je suis monté dans le train avec mon dû, mes deux chiens, mes bagages et mon frère. »


## De quoi tuer un bœuf
De Mandouri, les deux hommes partent à pied jusqu’au Monte Alto,
chez un certain Guillermo Bull, d’origine allemande, pour lequel le marquis Langbern leur a donné une recommandation.
Chemin faisant, à travers une savane arbustive qui portait bien son nom de « bosquets de l’enfer »,
Florian est piqué par des insectes non identifiés, mais dont les larves lui causeront de terribles démangeaisons dans le dos.
Quand Guillermo Bull le verra se trémousser sur sa chaise à table,
il s’inquiétera, lui demandera d’enlever sa chemise et deviendra blême en constatant l’étendue du désastre :
le dos de Florian est criblé de trous où sont logées de redoutables larves, du genre le l’œstre qui s’attaque au bétail de chez nous.
Il y en a bien une centaine, de quoi tuer un bœuf. Guillermo et Louis se mettent immédiatement à les extraire.
À leurs pieds, les poules sont ravies, mais n’arrivent pas à suivre.
Le temps n’est pourtant pas à la rigolade.
Le succès de l’opération est incertain.
Pour éviter l’infection, le dos du malheureux est passé à la saumure.
La brûlure est terrible.
Guillermo prend Louis à part : « Ton frère ne s’en sortira pas.
Il y a un menuisier pas très loin d’ici… » Louis a compris et s’en va chercher un cercueil.
Florian se rétablira et rejoindra au bout d’une quinzaine de jours le petit terrain de trois alguiéres, environ sept hectares, dénichés entre-temps par ses compagnons.
Les deux frères Portmann construisent une baraque et se lancent dans le défrichage et l’exploitation de leur domaine.
Des Italiens habitent non loin de là : Louis aime les rejoindre le soir pour discuter, jouer aux cartes.
Louis est costaud, mais fanfaron.
Un pari stupide tourne au drame.
Il prétend réaliser ce tour de force inconnu des haltérophiles : se mettre un sac de 60 kilos de riz sur l’épaule en étant assis par terre, jambes tendues.
Il a essayé, a poussé un grand cri et s’est effondré, les reins en compote.
« J’étais couché quand il est rentré, se souvient Florian.
Quelques heures plus tard, il m’a réveillé en me disant qu’il avait un problème : il ne pouvait plus uriner.
Il souffrait terriblement, geignait, me suppliant d’aller chercher un fachinere,
un de ces médecins de brousse soignant avec des plantes locales.
Je suis parti en pleine nuit, sachant vaguement où je pourrais en dénicher un.
À l’embranchement de deux pistes dans la forêt, je ne savais plus où me diriger ;
alors j’ai fait tourner mon cheval deux ou trois fois sur lui-même et je l’ai laissé aller.
Quelques kilomètres plus loin, il y avait une cabane.
C’était là.
J’ai tapé des mains, le gars est sorti.
Il m’a montré des fleurs violettes qu’on trouve en forêt.
« Prends-en une poignée que tu feras bouillir, m’a-t-il dit, et que ton frère fasse un bain de siège avec ça.
Donne-lui aussi à boire. » J’ai suivi ses indications et mon frère a pu très vite uriner : c’est du sang caillé qui est sorti.
Pendant des jours il est resté très faible.
Puis Guillermo a passé par là et l’a emmené chez lui pour que sa femme le soigne.
Il s’est rétabli au bout de deux ou trois mois. »


## Dans l’hôtellerie
Se retrouvant seul, Florian plante du coton, confie le domaine à un métayer suisse alémanique et retourne à Sao Paulo où il travaillera quelques mois comme soudeur dans la compagnie de tramway Light,
avant d’être engagé comme boucher à l’hôtel Esplanade, le plus grand établissement de la ville.
Il obtient cet emploi grâce à un Suisse nommé Bloch, dont il avait connu les cousins à Orbe, et qui était chef de la cuisine froide à l’Esplanade.
Florian travaillera deux ans et s’en ira sur un éclat, une dispute avec son ami Bloch.
Cela se passe dans les coulisses d’une grande réception.
Le chef de la cuisine froide laisse tomber une barre de glace sur un plat de filets de poissons préparé par Florian.
Celui-ci proteste dans un langage probablement fleuri, l’autre le bouscule, très énervé.
Le ton monte, Florian, qui a un grand couteau à la main, coupe d’un seul coup tous les boutons de l’habit du petit gros rougeaud, qui devient blanc.
« Je n’avais plus qu’à demander mon compte et à partir », conclut Florian en riant encore de la scène.
Entre-temps le domaine de Mandouri avait été vendu.
Le métayer ayant présenté des comptes douteux après la récolte de coton, Florian n’a pas voulu renouveler une expérience décevante.
Quant à son frère Louis, après son accident, il était resté chez Guillermo Bull, et lorsque celui-ci décéda, il épousa sa veuve.
Florian retourne travailler à Araras, comme étameur cette fois-ci.
Il restera moins d’une année, finissant par se disputer avec le chef-ferblantier Grossmann qui lui proposait une augmentation de salaire s’il lui rapportait les propos tenus à son égard dans l’atelier.


## Officiers d’opérette
Notre cow-boy solitaire reprend la route de Sao Paulo, où il loue une chambre chez une Allemande de la rue du 7-Avril.
Oh, pas pour longtemps !
Un ami jurassien, Jean Gros, le met en contact avec un trio infernal composé du général Pain,
propriétaire d’une fazenda dans le Mato Grosso, du capitaine Guasque, gaucho à la fazenda de Rio Negro, et Mario Morel, d’origine française.
Les trois lascars s’étaient associés pour acheter un saladeiro,
une installation pour sécher la viande, et voulaient monter des équipements frigorifiques à Aquidaouana, près de Campo Grande, dans le Mato Grosso do Sul.
N’ayant pas de capitaux, ils avaient acheté à crédit et pensaient réaliser de bonnes affaires.
Florian est l’homme qu’il leur faut pour mettre en route leur entreprise.
Ils l’engagent. Florian se rend donc à Aquidaouana.
« Quand je suis arrivé sur place pour monter les machines, elles n’étaient pas là et les installations du saladeiro étaient dans un état lamentable.
J’ai réparé la chaudière de la machine à vapeur, le rhéostat de la génératrice, j’ai remonté le château d’eau de deux mètres pour augmenter la pression, mis une pompe dans la rivière avec un réservoir.
Enfin, j’ai travaillé de nombreux mois, sans être payé.
Finalement je suis allé me plaindre auprès d’un agent du Ministère du travail.
La société avait été démantelée et il n’y avait pas de responsable.
Je n’ai donc jamais été payé. » Sa plainte n’a pas été du goût de deux acolytes du trio,
délégués sur le chantier, le vacher Antonio di Abreu et le charpentier Manuel de Frete de Bittencourt, qui supervisaient les travaux.
Mesure de rétorsion : ils emmenent les trois vaches qui fournissaient le lait de la petite communauté.
La vengeance vise Florian, qui a besoin de lait pour le veau qu’il a recueilli et qui est devenu son compagnon préféré.
« Je l’avais trouvé le 1er janvier, couché devant ma porte.
Il était plein d’asticots sur tout le corps.
Pendant dix-huit jours je l’ai porté dans les bras.
Un boucher d’Aquidaouana, qui avait vu la vache vêler, fiévreuse et impropre à la consommation, s’était empressé de la tuer et avait laissé le veau dans le pâturage. »


## Un veau en guise de chien
« Quand j’ai voulu lui donner son premier lait, poursuit Florian visiblement ému, il ne pouvait pas ouvrir la gueule tant il avait d’asticots autour des dents.
Cette pauvre bête ! J’en aurais pleuré…
J’ai pris une feuille creuse de mamon et en ai fait une paille.
Je l’ai alimenté ainsi pendant plusieurs jours, tout en lui massant la mâchoire avec de la térébenthine pour qu’il puisse l’ouvrir.
Plus tard, quand on n’a plus eu de lait, j’ai continué à le nourrir avec des oranges douces que j’épluchais. »
Le veau est devenu comme un chien pour Florian, le suivant partout, allant jusqu’à le rechercher au café de la station de chemin de fer,
à un kilomètre de la maison, un soir de bal ! Ils n’ont pas dansé ensemble, mais l’anecdote a amusé, on l’imagine, tout la contrée.
Ayant abandonné le saladeiro, il n’est pas question cette fois pour Florian de retourner à Sao Paulo : il ne va tout de même pas abandonner son veau.
Il travaille alors pour un Italien, du nom de Lockman, qui a acheté une usine électrique près de Santos et veut la démonter,
la transporter par chemin de fer et la remonter à Aquidaouana, avec sa turbine à vapeur Delaunay-Belleville, une merveille du genre.
Notre homme aux multiples talents s’acquitte de cette tâche, tout en jouant les radiesthésistes pour trouver l’eau nécessaire à proximité et aménager un puits.
Ce contrat rempli, il est engagé par un ami noir, l’avocat José Sabine, pour aller relever une fazenda abandonnée depuis une trentaine d’années non loin d’Aquidaouana.
Florian accomplit ce nouveau mandat avec l’aide de quelques employés et de… son veau.
Deux ans plus tard, il quitte la fazenda, aux clôtures neuves, ayant marqué 255 têtes de bétail du signe de José Sabine.
Celui-ci est enchanté : sa propriété a doublé de valeur, il va pouvoir la revendre avec un substantiel bénéfice.
Là-dessus Florian part pour Corumba, dans le Mato Grosso, laissant son veau aux bons soins d’un ami fermier d’Aquidaouana,
Gine Mulaton, un immense gaillard, petit fabricant de cachaça à ses heures et marié avec une Indienne du Paraguay, boulotte et vaguement sorcière.
À Corumba, il se met enfin à son compte, comme réparateur de machines en tous genres et d’installations électriques.
« Un jour, j’ai rencontré par hasard la femme de Gine.
Elle m’a annoncé que son mari était mort, sans m’avouer qu’elle l’avait empoisonné parce qu’il la trompait…
Je lui ai demandé bien sûr des nouvelles de mon veau.
C’était devenu une jeune vache qui venait de vêler ! » Le cœur du sensible Florian ne fait qu’un bond.
Il prend le train pour Aquidaouana et confie la vache et son veau au responsable d’une des fazendas de José Barboso,
un gros propriétaire de ses amis qui possède la bagatelle de sept fazendas dont la plus petite est d’une lieue carrée, soit 360 hectares…
Il donne également sa marque pour la descendance, ayant convenu que les femelles lui reviendraient.
« Des années plus tard, José Barboso est mort et j’ai été convoqué par son demi-frère : j’avais droit à 45 têtes de bétail.
Il m’a offert un compte de reis et demi pour chacune ; il a signé un chèque et je suis allé retirer le blé à la banque.
Ce veau moribond que j’avais recueilli m’avait bien remercié… » C’est à cette époque que Florian,
décidément polyvalent, va participer à une boyade, une de ces impressionnantes transhumances de quelques 5000 bœufs sauvages.
Il a été invité, avec des amis français, à une fête donnée à la fazenda Santa Rosa,
ce genre de fête dont raffolent les éleveurs sud-américains et qui peuvent durer une semaine :
c’est la churrasca permanente, trois ou quatre bœufs quotidiennement tuées, grillés, mangés et bien arrosés dans l’allégresse générale.
Un des convives, Decoros Ortiz, a un problème de convoyage de cinq mulets de selle jusqu’à Aquidaouana, huit jours de voyage.
Florian offre ses services.
À cette époque, il n’y avait pas de clôtures entre les fazendas, on les traversait en suivant la direction générale plutôt que les routes.
Un soir, il arrive près d’une bâtisse de la fazenda Protection.
Il frappe dans ses mains avant de mettre pied à terre, comme il est d’usage si l’on ne souhaite pas recevoir une balle en guise de salutations.
Une femme paraît.
Il lui demande s’il peut s’arrêter là pour la nuit.
« C’est bien que tu sois arrivé, lui dit-elle, car il y a une susuaran, qui vient me tuer un veau chaque soir.
Or je suis seule avec les enfants… » Son mari participait au rodéo, le rassemblement du bétail pour le marquage ou la vente.
La susuaran ? C’est un fauve de la famille des pumas.
Elle tend une carabine et une balle à Florian : « Un gosse m’a montré le chemin.
La susuaran était sur un arbre, au pied duquel une meute de chiens aboyait.
Je l’ai visée et abattue.
Elle est tombée à terre, s’est débattue et a pu encore arracher les boyaux d’un chien qui lui sautait dessus.
J’ai récupéré le cuir, la moitié de la viande et donné l’autre moité à la femme.
Je suis parti deux jours plus tard. »


## 5000 bêtes à convoyer
Arrivé à Aquidaouana, il trouve un Decoros Ortiz ravi, qui lui propose de l’accompagner au Pantanal, région d’élevage, pour une boyade, Florian accepte.
« On réunit 5000 bêtes dans un corral, qu’on laisse durant cinq jours sans boire ni manger, sinon il n’y a personne qui puisse dominer un tel troupeau, explique Florian.
Quand on ouvre la porte du corral, le cornetteur va devant et cornette sans cesse pour habituer les bêtes à ce son, qui va les guider.
Il les dirige vers le premier point d’eau, où les bœufs vont pouvoir s’abreuver.
Le premier jour, on ne fait guère que six kilomètres.
Puis les étapes s’allongent, le cornetteur accélère le rythme.
Au début, il faut au moins 80 cowboys pour encadrer le troupeau ; après c’est plus facile, il est plus discipliné.
Mais quand les bœufs sentent une susuaran ou une bête puante, la boyade éclate, et pas question d’essayer de les retenir :
vous seriez renversé avec votre mulet et piétiné ! »
L’immense troupeau traverse les fazendas ramassant au passage le bétail qui se trouve sur sa route, mais perdant aussi des bêtes qui filent à l’anglaise.
C’est le principe sans scrupule des vases plus ou moins communicants,
l’essentiel pour les hommes de la boyade étant d’arriver au terme du voyage de trois mois avec le même nombre de bêtes qu’au départ.
Elles retrouveront dans de vastes pâturages le poids perdu au cours de la longue transhumance.


## Steak tartare
Une charrette d’intendance tirée par quarante bœufs précède la boyade.
Le charretier connaît les étapes.
Chaque matin avant le départ, un bœuf est tué et chacun reçoit une part coupée en fines tranches.
Classiquement la viande crue est mise sous la selle : elle sera cuite par la chaleur du mulet, salée par sa transpiration.
Entre deux bivouacs, les hommes n’ont qu’à tirer une tranche de dessous leurs fesses, racler les poils de mulet avec un couteau et la déguster avec de la farine de manioc.
Florian, qui n’est pas un douillet, goûte cette vie-là et participe à plusieurs boyades jusqu’au jour où, sur le chemin du retour,
il traverse avec ses compagnons la fazenda Protection et passe devant la maison où il avait tué une susuaran.
La femme le reconnaît et rappelle l’anecdote à son mari et à tous les cowboys.
Les commentaires équivoques ne tardent pas et les plaisanteries douteuses fusent.
Cela durera plusieurs jours.
Florian se fâche : ce sera sa dernière boyade, en dépit de l’insistance de Decoros qui veut lui confier la responsabilité de la suivante.
« Je lui ai dit : ce n’est pas possible, s’ils continuent leurs sales insinuations,
je vais en tuer un ! » Florian gardera le surnom de Susuaran, le puma, ce qui lui vaudra le respect, voire la crainte, de plus d’un pistolero.
Florian n’aimait pas trop ce sobriquet, qui lui a cependant été quelques fois utile.
Florian se lance alors dans une nouvelle aventure : il veut aller chercher des diamants dans le Mato Grosso,
il sera garimpeiro. Cette idée lui trotte depuis longtemps dans la tête : le moment est venu de la réaliser.
« J’ai pris le vapeur qui remonte le rio Paraguay vers Cuiaba, où j’ai laissé mes affaires chez un ami, Georges Pommot, et je me suis enfilé dans les garimpos de diamants. »


## Sauvé par un Indien
« J’ai été garimpeiro muito tanto… » Le ton a changé et toute la dureté de cette vie transparaît dans la voix.
« J’étais châtain et mes cheveux sont devenus blancs.
J’ai eu des fièvres terribles.
Je suis resté huit jours sans une goutte d’eau, avec la fièvre et la maladie du sommeil, la coluchon, comme on l’appelle ici.
Elle attaque la moelle épinière et l’on reste prostré comme une poule couvant ses œufs, jusqu’à la mort si personne ne s’occupe de vous.
Un jour, un Indien de la tribu des Cheben a surgi et m’a trouvé ainsi, déjà inconscient, presque mort.
Et il m’a soigné, avec des plantes, des racines, des mixtures dont il avait le secret.
Le premier soir, il est parti après m’avoir frictionné et fait boire quelques potions.
Le lendemain matin, il était de retour et m’a transporté jusque dans sa baraque.
Bartoli, c’était son nom, avait été expulsé de sa tribu pour deux ans, après avoir commis je ne sais plus quelle faute.
Il avait fait une rossa dans la forêt, défriché une parcelle pour y planter du maïs et du manioc.
Je ne comprenais pas la langue de sa tribu, dont je connaissais une particularité :
ils ne frappent pas dans leurs mains quand ils arrivent quelque part, comme on le fait ici ; eux, ils frappent du pied par terre.
Je suis resté trois mois avec Bartoli.
Sous l’effet de la maladie et de la fièvre, j’avais la plante des pieds complètement pelée ; je suis aujourd’hui encore terriblement sensible.
L’Indien m’a ramené finalement à Gatigne, ville de garimpeiros devenue aujourd’hui Alto Paraguay.
Une carte de mon ami Pommot m’y attendait qui disait : « J’ai appris que tu étais mort et ne veux pas le croire.
Si tu es vivant, reviens vite Cuiaba : notre maison est la tienne »… » Florian descend à Cuiaba en camion.
Quand il arrive chez son ami, celui-ci ne le reconnaît pas tout de suite : il faisait 86 kilos lors de leur dernière rencontre,
le revenant filiforme n’en pèse plus que 47, la peau et les os.
Quand il comprend, Georges Pommot fond en larmes.
Après trois ou quatre mois de convalescence, Florian repart « garimper », non pas à Gatigne,
mais à une cinquantaine de kilomètres seulement au nord de Cuiaba, dans la région de Guia.
À la base, le principe est toujours le même, chaque homme a son « front de service » d’une quinzaine de mètres, qu’il peut exploiter jusqu’à l’infini.
Avec toutes les querelles de voisinage qu’on imagine lorsqu’un filon part à gauche ou à droite.
Les querelles, les arnaques et les sales coups sont monnaie courante dans ce monde sans pardon des chercheurs d’or ou de pierres précieuses.
À un certain moment, Florian peut acheter un scaphandre, pour opérer dans la rivière ; puis il constitue une équipe :
il met à disposition son matériel et nourrit les hommes qui travaillent pour lui.
Il veut aller plus loin encore, en homme d’action qu’il est.


## Expédition en Amazonie
Il organise une expédition en Amazonie, prenant huit hommes avec lui et sa chargeant de tous les frais.
Un avion les emmène jusqu’à Porto Velho, sur le Madeira, gros affluent de l’Amazone.
En camion, puis en bateau, ils remontent le rio Jamari sur Ariquemes, louent une troupe de mulets pour atteindre Rondonia, appelé aujourd’hui Jiparanà,
comme la rivière qu’ils redescendront après avoir « garimpé » en amont dans le rio Machado.
« J’avais récolté déjà pas mal de diamants, au moins 45 carats, se souvient Florian. Avec moi, j’avais un Suisse, Benjamin Steingruber, un énorme bougre qu’on appelait Tarzan.
C’était un excellent plongeur, mais il aimait bien se bigorner, se quereller.
Comme il y avait beaucoup de garimpeiros dans le coin, j’ai eu peur qu’ils le tuent et j’ai préféré qu’on aille plus bas avec le scaphandre.
On a fait une pirogue et on est descendu.
Quand on est arrivé à proximité des rapides, on s’est accroché aux branches des arbres bordant le rio.
Tarzan, qui pilotait, s’est dressé et a lâché : on peut passer.
J’ai dit : je vois de l’écume qui gicle très haut ! Il a crié : c’est rien, lâchez les branches ! On est parti comme une balle.
Avec nos rames on n’a rien pu contrôler.
On a volé par-dessus la chute qui devait faire huit à dix mètres, on a perdu notre vitesse et on s’est retourné dans la chute suivante. »
On avait tout perdu, poursuit tristement Florian : le scaphandre, tout notre matériel, nos provisions, nos allumettes.
Je n’ai sauvé que mon fusil, qui était attaché à la pirogue, et on a récupéré des boîtes d’avoine et de graisse.
Les diamants ? Perdus aussi : ils étaient dans ma casaque que j’avais retirée parce qu’il faisait chaud et que je n’ai pas retrouvée… »
La suite de l’expédition tourna à la Bérézina tropicale.
Il fallut quinze jours à la misérable équipe pour atteindre le premier serigal (exploitation de caoutchouc) au bord du rio.
Environnement hostile avec de sérieux avertissements fléchés des Indiens Arraras, qui interdisaient certains bras du rio.
Les hommes avaient survécu en mangeant un mélange cru de graisse et d’avoine, mais ils étaient fiévreux de malaria.
Et ils se trouvaient encore, sur une embarcation pourrie, à des centaines de kilomètres de Tabajara, où ils espéraient dénicher un bateau pour les ramener à Porto Velho.
Heureusement, le propriétaire du serigal, Barros, fut généreux.
Ils purent se refaire une santé et repartir dans de bonnes conditions,
avec une lettre de recommandation à l’attention du gouverneur pour leur faciliter un passage en avion jusqu’à Cuiaba.
« Ainsi, conclut Florian, après des mois de pérégrinations et d’efforts, on avait tout perdu et on se retrouvait au point de départ avec quelques dettes. »
Florian reprend modestement son activité de garimpeiro sur sont « front de service » de quinze mètres de large, près de Guia.
Il y restera encore bien des années, en y trouvant des diamants certes, mais surtout une perle : Dona Eve, sa femme.


## Le mariage du garimpeiro
La première fois qu’il l’a vue, elle était à cheval, un lasso à la main, à la recherche de bétail.
Florian put lui annoncer qu’il avait vu passer une bête portant telle marque.
C’était bien ça. Il revit plusieurs fois cette femme énergique et digne ; du sang indien coulait dans ses veines.
Il ne lui fut pas facile de la courtiser, car l’oncle et la tante, qui l’élevaient dans leur fazenda de Cochipoassou depuis la mort de ses parents,
ne voulaient pas qu’elle fréquente un de ces garimpeiros dont la réputation était mauvaise.
Une autre tante, Dona Marequigna, s’en mêla heureusement.
C’était une femme de grande influence et fort respectée, qui tenait les clefs du coffre aux joyaux portugais de l’église.
Elle avait beaucoup d’admiration et de reconnaissance pour Florian qui avait gentiment aidé son fils à monter une forge, lui enseignant diverses techniques.
Son intervention se révéla décisive.
Le mariage civil fut célébré devant le maire de Guia et l’on attendit le passage d’un prêtre pour la cérémonie religieuse.


## Accouchements difficiles
Florian avait 48 ans, Dona Eve 23 ans, et de leur union devaient naître Helvetia, en 1954, Blanche-Neige, deux ans plus tard, puis Guillaume Tell.
Les accouchements furent à chaque fois difficiles.
Le couple habitait dans la cambuse reculée du garimpeiro.
Pour l’aînée, les difficultés rencontrées par Dona Eve obligèrent Florian à aller chercher en catastrophe, en pleine nuit,
à quelques kilomètres dans la forêt et sous des trombes d’eau, une vieille sage-femme, qu’il dut même porter dans ses bras pour traverser des cours d’eau en furie.
Celle-ci comprit au chevet de Dona Eve que la situation dépassait ses compétences.
Il fallait faire venir une ambulance et transporter la parturiente à l’hôpital de Cuiaba.
On la porta tout d’abord jusqu’à la route fort éloignée dans un hamac suspendu à une perche.
L’ambulance ne vint pas, elle avait été réquisitionnée pour une tournée électorale.
Quand Dona Eve arriva à l’hôpital, il fallut avoir recours aux fers et elle fut délivrée deux heures plus tard.
Pour la petite Blanche-Neige, qui deviendra plus commodément Bianca dans la bouche de ses proches, le scénario débuta de la même manière,
mais la vielle sage-femme put exercer ses talents jusqu’au bout et l’enfant vint au monde dans une école inoccupée,
à deux pas de la « maison » que Florian était en train de construire pour sa famille.


## Un drame
Et puis, il y aura un drame.
Dona Eve attendait un troisième enfant.
« C’est de ma faute, s’il est mort, lâche Florian, la voix brisée par le chagrin.
Je n’aurais jamais dû la laisser…
Une machine à monter, un travail urgent : j’avais dit, je ne peux pas y aller, ma femme va accoucher.
Mon voisin avait alors proposé de la prendre chez lui, sa propre femme pouvant s’occuper d’elle.
Un soir, leurs enfants ont voulu faire une farce en se barbouillant le visage et en entrant précipitamment dans la chambre où Eve se reposait, avec Bianca.
La petite a eu peur et a hurlé.
Eve s’est retournée brusquement.
Dans son ventre, le bébé n’a pas supporté ce choc.
On est venu me chercher sur le chantier :
Ta femme est à l’hôpital et ne va pas bien.
J’ai pris un bus.
Le médecin avait fait une césarienne.
Le petit Guillermo Tell a vécu trois jours. »
Plus tard, en 1964, Eve et Florian adopteront un garçon, un bébé de deux semaines, emballé dans des journaux, malade et abandonné devant leur porte, le premier jour de l’an.
Ils lui donneront ce même prénom de Guillermo Tell.
Entre-temps, des événements dramatiques et rocambolesques ont eu lieu dans le contexte western des impitoyables et violents garimpeiros.
Près de Guia, le chef de famille qu’est devenu Florian a installé les siens dans une masure un peu plus confortable que sa baraque de célibataire.
Il possède des chevaux, une vache et du menu bétail, mais vit de son travail de garimpero :
lavage incessant des cailloux, des graviers, les tamis de différentes grosseurs qu’il faut tourner des deux mains avec parfois les pierres précieuses,
plus lourdes, qui restent au centre.
La région est désormais connue et c’est la ruée.
On se bouscule.
Florian a de la chance, ses voisins n’en ont pas.
Deux frères d’origine italienne, installés depuis peu juste à côté de sa zone de prospection,
ne trouvent pratiquement pas de diamants et se mettent à empiéter sur le domaine de Florian, contestant les limites.
Ce type de confit est quotidien, tourne court ou se termine mal.
Avertissements, engueulades, on se dispute des tas de graviers et on en vient aux mains.
Florian n’est pas du genre mauviette et ses voisins déguerpissent piteusement, humiliés et furieux.
Un jour Florian va faire des achats à Guia, et c’est la chronique d’une tentative d’assassinat annoncée.
Les deux Italiens ont clamé qu’ils lui feraient la peau.
L’occasion se présente.
Pendant qu’il fait ses courses, qu’il discute dans un magasin avec un oncle de sa femme, Sirio, puis avec un autre parent à la boulangerie, l’étau se resserre.
Il ne manque que la musique d’Ennio Morricone.
Florian n’est pas né de la dernière pluie et sait se servir de son pistolet quand il s’agit de sauver sa peau ;
il l’a à portée de main, sûreté déverrouillée.
Quand il traverse la place pour rejoindre Sirio qui l’attend à la churrascaria, il est pris en tenaille par les Italiens.
En moins de temps qu’il n’en faut pour le dire, un Italien s’écroule, blessé au flanc et aux jambes, l’autre aussi, touché à un bras et à une jambe.
Florian n’a pas une égratignure : ses assaillants avaient rayé leurs balles en croix,
pour qu’elles aient plus d’effet à l’impact, mais oublié qu’à distance la précision du tir devenait nulle.
Lorsque Florian saisit à la gorge son adversaire le moins atteint et s’apprête à l’étrangler,
celui-ci réussit à lui tirer à bout portant une balle dans la tête.
« Touche là, il y a encore le trou ! » Trente ans plus tard,
le beau vieillard miraculé porte toujours les stigmates, entre le front et le sommet du crâne :
un sillon dans lequel on peut en effet mettre le doigt ! Les trois blessés sont emmenés à l’hôpital de Cuiaba.
Les deux frères Italiens en sortiront très vite et pourront faire jouer leurs relations pour charger Florian,
qui sera inculpé de tentative de meurtre.
Il est toutefois prématuré de l’incarcérer et de le traduire en justice, car il est intransportable.
On l’a trépané et on lui a retiré trois morceaux de balle de calibre 44.
Deux agents de police sont placés en sentinelle à la porte de l’hôpital.
Une chance pour le malheureux : un de ses amis, le père Pedro Lachat, d’origine jurassienne évidemment,
est alerté et actionne la sonnette de la solidarité.
Florian a des ennemis mortels dans la cité, mais aussi des amis indéfectibles, jusque dans la police.
D’un côté, certains ont juré que cet ennemi public numéro un devait être envoyé d’une manière ou d’une autre dans l’au-delà,
en respectant ou non la procédure judiciaire, peu importe !
D’autres, parfaitement au courant de ces funestes intentions, étaient déterminés à l’arracher de ce mauvais pas.
Ces derniers réussiront dans leur tentative avant que les premiers ne puissent mettre leurs projets à exécution.


## Évasion spectaculaire
C’est ainsi que Florian, à peine valide, pourra s’échapper de l’hôpital en taxi grâce à son ami Lachat, à la barbe de tous.
Puis il sera caché chez Georges Pommot, transfert rendant infructueux la perquisition opérée dès le lendemain chez le Jurassien.
Enfin, le major Maier, son copain commissaire de police, lui fera remettre à toutes fins utiles un pistolet et des balles,
avant d’organiser lui-même, pour faciliter son départ en avion vers l’État voisin de Goias, une sensationnelle manœuvre de diversion :
il déclenchera une évasion collective de la prison de Cuiaba, qui mobilisera toutes les forces de police.
Pendant ce temps, déguisé et muni d’un billet portant un faux nom, Florian s’envolera pour Campo Grande, dans le Mato Grosso do Sul,
prendra un train pour Tres Lagoas, remontera à Cassilandia pour passer enfin dans l’État de Goias et rejoindre Jatai.
Ce très grand détour avait une raison : sur la voie directe, la frontière avait été fermée pour empêcher le départ du dangereux personnage.
Florian travaillera à Jatai pour un fabricant de moulins et de machines à décortiquer le café et le riz, un Carloni qui était son voisin à Guia.
Il ira deux mois plus tard récupérer sa femme et ses enfants réfugiés chez le père Lachat à Cuiaba,
expédition audacieuse dans un État où il était toujours considéré comme l’ennemi public numéro un et recherché en conséquence.
Mais Florian a décidément un ange gardien et il s’en tirera une fois de plus.
Jatai sera l’avant-dernière étape de son vaste périple brésilien.
Il y restera trois ou quatre ans puis, très naturellement, il sera attiré par un immense chantier dont on parle beaucoup :
la construction de la nouvelle capitale, Brasilia.
Le futur district fédéral est un périmètre enclavé dans l’État de Goias, à 500 kilomètres à vol d’oiseau.
Il ne résistera pas à cet appel.
En 1958, il installe sa famille dans un hangar de Taguatinga, cité satellite de Brasilia, encore à l’état de vaste terrain vague.
Il obtient une concession pour construire une baraque de planches, la première d’un nouveau lotissement.
Il mettra quinze ans pour terminer sa maison.
Il travaille à gauche et à droite, dans tous les domaines, qui sont autant de spécialités pour lui :
hydraulique, électricité, soudure, mécanique, etc.
Il participe à l’achèvement de la construction de l’hôpital de Taguatinga,
dont la sœur directrice lui sera longtemps reconnaissante d’avoir, en prime, réparé le toit endommagé de la chapelle.
Elle s’en souvient en 1960 lorsqu’elle est transférée à l’hôpital Sarah Kubicek : elle fait engager Florian dans ce grand établissement de la capitale,
le plus important hôpital orthopédique d’Amérique latine.
Vingt-huit ans plus tard, il y sera encore…
Il monte l’atelier orthopédique, dont il sera responsable.
On l’envoie se perfectionner à Sao Paulo.
« La première prothèse que j’ai fabriquée, se souvient Florian, c’était pour un jeune Suisse de Nova Friburgo ! »
Il s’occupe aussi de la maintenance des appareils des installations électriques.
Quand sonne l’heure de la retraite, il quitte l’hôpital comme le veut la loi.
Il effectue des travaux à l’ambassade de Suisse, durant quatre mois, jusqu’à ce que le directeur de l’hôpital,
le Dr Aloysio Campos da Paz, le rappelle.
Une autre règle est « oubliée », celle qui veut que deux membres de la même famille ne puissent être simultanément employés à Sarah Kubicek.
Mais Florian, considéré comme le bon Dieu incarné dans cet univers hospitalier, est définitivement une exception.
Ainsi le veut la direction, approuvée par le personnel.


## Un conteur à l’hôpital
Comme il a formé son successeur, « un jeune qui était trop intelligent pour manier seulement le balai »,
Florian ne reprend pas du service à l’atelier orthopédique, il s’occupe de maintenance.
Mais il est bientôt opéré d’une hernie, puis d’une deuxième hernie, et le médecin lui interdit de grimper sur des échelles et de porter des charges.
Alors, ce qu’il faisait de temps à autre devient son activité principale :
il raconte des histoires aux enfants, et ce grand-père devient le rayon de soleil quotidien des petits handicapés.
On les réunit dans l’infirmerie et dès qu’il arrive, fusent les « papa Noël ! papa Noël ! »
La plupart sont là pour des longs séjours, parfois à la suite d’un accident ou en raison d’une malformation, d’une paralysie.
Quand Florian fait la tournée des chambres, tous les visages s’illuminent et il n’est guère que les malheureux hydrocéphales,
aux têtes trop lourdes pour se retourner à son passage, qui ne se réjouissent pas visiblement de sa présence.
Et le roi des enfants passe de l’un à l’autre en murmurant une bénédiction.
Florian est devenu père Noël officiel de Brasilia dans les années 70 en répondant à une annonce.
« Des collègues de travail m’ont poussé ; ils étaient sûrs que j’allais être choisi.
Comme j’aimais beaucoup les enfants, je me suis présenté et après deux tests, j’ai été retenu parmi une vingtaine de candidats.
Au début j’étais tout seul, mais ça représentait trop de travail durant presque tout le mois de décembre, avec parfois quatre représentations par jour.
Maintenant j’ai des aides. » Le père Noël de Brasilia va distribuer des bonbons,
des cadeaux et bénir les enfants dans tous les quartiers de la capitale et des cités du district fédéral.
Il se déplace toujours en hélicoptère.
« Un jour on a eu une panne et on a dû atterrir en catastrophe derrière le palais présidentiel.
Les gardes ont failli nous abattre en croyant à un coup d’État ou à un enlèvement.
Le pilote est sorti en hurlant : arrêtez, c’est papa Noël !
Le président a tout de suite mis un autre hélicoptère à notre disposition. » Au terme d’une vie très mouvementée,
faite d’aventures, de violence, de bagarres parfois sanglantes, le voici dans un monde de paix.
Ce qui émane de ce vieillard, qui, pour arriver à cet âge-là, dut à l’occasion tirer plus vite que son vis-à-vis, c’est un grand calme intérieur et une infinie douceur.
C’est un homme très sensible, qui à facilement la larme à l’œil, en présence ou à l’évocation de la misère humaine.
S’il faut une preuve supplémentaire de la nature fondamentalement généreuse de Florian, il suffit d’entrer dans sa maison.
Qui trouve-t-on ? Blanche-Neige, qui vit encore pour quelque temps avec ses parents - Helvetia et Guillaume Tell, eux ont déjà quitté le nid.
Mais qui est cette petite de treize ans, en train de faire ses devoirs alors que Dona Eve prépare le repas dans la cour ?
C’est Michèle, la fille d’une pauvre muette qu’ils ont recueillie un jour de pluie ;
celle-ci traînait dans la rue, expulsée de chez elle, parce qu’elle était enceinte.
Personne n’en voulait plus.
Ils l’ont gardée, et son bébé avec.
Au bout de deux ans, la mère est partie ; elle passe de temps à autre.
Michèle est restée bien sûr : « Je l’ai eue dans mes bras dès le jour de sa naissance, dit Florian en la regardant gentiment.
On est responsable d’elle jusqu’à ses dix-huit ans. » Et ce petit bout de chou, qui trottine joyeusement et met sans cesse son grain de sel dans la conversation ?
C’est Mariette.
Sa mère l’a abandonnée à une voisine pour aller vendre ses charmes dans la Serra Pelada, où le travail ne manque pas.
« Mais cette voisine travaillait et ne pouvait pas s’occuper de cette petite, explique Florian, des sanglots dans la voix.
Elle restait seule toute la journée et ça a duré plusieurs mois.
Ma femme a eu pitié d’elle et la prise.
Elle est avec nous maintenant.
Elle est mignonne, non ? Pauvre fille ! » Florian secoue la tête en regardant la petite fille au sourire éclatant :
« C’est magnifique de pouvoir s’occuper des enfants comme ça.
Mais combien sont-ils à vivre une enfance de tourments et de misère dans le monde.
Ça fait pitié.
Les gens se disent humains, mais finalement, je n’en ai pas rencontrés beaucoup. »

---
## Description des images dans l’ordre de leur apparition

À l’automne d’une vie de 80 ans particulièrement dense et aventureuse, Florian Portmann,
dans la cour-cuisine de sa petite maison d’une cité satellite de Brasilia,
raconte une histoire à la petite Marieta, une adorable gosse qu’il a recueillie.
Un petit mot encore à Dona Eve, sa femme, et à Michèle, et il part en ville, poursuivre sa noble tâche : égayer la vie de pauvres gosses hospitalisés.

Pour rejoindre le centre d’une capitale créée de toutes pièces, il faut faire de longs trajets en bus depuis les cités satellites.
Mais c’est aussi l’occasion de multiples rencontres. Florian est très populaire dans le district fédéral ;
ici c’est le boucher du coin de la rue qui l’embrasse,
plus loin les enfants se précipiteront vers lui, en l’appelant « papa Noël ! »

L’hôpital Sarah Kubicek de Brasilia est le plus grand établissement orthopédique d’Amérique latine.
Quand Florian le conteur arrive, on réunit les enfants dans l’infirmerie ;
beaucoup sont là pour de très longs séjours, victimes d’un accident, d’une malformation, d’une paralysie.
La présence de ce grand-père extraordinaire et les fabuleuses histoires qu’il raconte illuminent les jours sombres des petits handicapés.

Père Noël officiel de Brasilia depuis les années 70, Florian a un mois de décembre extrêmement chargé.
Un hélicoptère mis à sa disposition lui permet de multiplier ses interventions aux quatre coins du district fédéral,
dans les banlieues éloignées et les bidonvilles.
Sa notoriété est si grande, et sa barbe blanche naturelle si célèbre que ses fans le reconnaissent même lorsqu’il a enlevé son costume.
Tout au long de l’année où qu’il aille,
il ne se passe pas un jour sans que quelqu’un, de sept ou de septante-sept ans, ne l’interpelle d’un joyeux « papa Noël ! »
