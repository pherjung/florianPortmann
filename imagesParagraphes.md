# 60 ans d'aventures en Amérique du Sud
## Bain de foule assuré
- Brasília

## Un roman d'aventure
- Brasília
- Suisse (Vaud)

## À Saint-Saph: clients médusés
- Lavaux
- Saint-Saphorin (Vaud)

## Explosion à Paris
- Paris

## Le grand-père américain
- Neuchâtel
- Val-de-Travers
- État de New-York

## Cabotage jusqu'à Sao Paulo
- Havre
- Rio de Janeiro

## Vigne anglaise à tailler
- São Paulo
- Araras

## Lavage de boilles
- Araras

## Le beurre et le miel
- Araras <-> Rio Claro
- Appezell (mentionne)
- Jura (mentionne)
- Dénériaz
- Itipucatu
- Manduri (mentionne)

## De quoi tuer un bœuf
- Manduri
- Bosquets de l'enfer

## Dans l'hôtellerie
- São Paulo
- Hôtel Esplanada
- Manduri (mantionne)
- Araras

## Officiers d'opérette
- Direction São Paulo ?
- Mato Grosso (mentionne)
- Rio Negro (?) (mentionne)
- Aquidaouana

## Un veau en guise de chien
- Aquidaouana
- São Paulo (mentionne)
- Santos (mentionne)
- Aquidaouana
- Corumba
- fazenda Santa Rosa
- fazenda Proteção

## 5000 bêtes à convoyer
- Aquidaouana
- Pantanal

## Steak tartare
- Pantanal
- fazenda Proteção
- vers Cuiaba, chez Georges Pommot (il me semble avoir vu des lettres de cette personne, si ça se trouve, il n'a jamais déménagé)

## Sauvé par un Indien
- Lieu où habite la tribu Cheben
- Gatigne/Alto Paraguay
- Cuiaba
- Gatigne

## Expédition en Amazonie
- Amazonie
- Porto Velho
- Rio Madeira
- remontant le rio Jamari vers Ariquemes
- Randonia/Jiparanà (en mulet)
- Rio Machado
- où habite la tribu Arraras
- Tabajara (mentionne)
- Porto Velho (mentionne)
- Cuiaba
- Près de Guia

## Le mariage du garimpeiro
- fazenda de Cochipoassou

## Accouchements difficiles
- Hôpital de Cuiaba

## Un drame
- Brasília (mentionne, sans citer le nom de la ville)
- Guia
- Hôpital de Cuiaba

## Évasion spectaculaire
- Hôpital de Cuiaba
- Chez Georges Pommot
- État de Goias (mentionne)
- prison de Cuiaba (mentionne)
- Campo Grande
- Três Lagoas
- Cassilandia
- Jatai
- Guia (mentionne)
- Chez le Père Lachat à Cuiaba
- Jatai
- Brasília (mentionne)
- Taguatinga
- Hôpital Taguatinga
- Hôpital Sara Kubitscheck
- São Paulo
- Nova Friburgo (mentionne)
- Ambassade Suisse
- Hôpital Sara Kubitscheck

## Un conteur à l'hôpital
- Hôpital Sara Kubitscheck
- État de Brasilia
- Palais Présidentiel
- État de Brasilia
- Taguatinga
